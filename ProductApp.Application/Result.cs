﻿namespace ProductApp.Application
{
    public class Result<T>
    {
        public T Data { get; }
        public bool IsSuccess { get; }
        public string Error { get; }

        public Result(T value)
        {
            Data = value;
            IsSuccess = true;
            Error = null;
        }

        public Result(bool isSuccess, string error)
        {
            IsSuccess = isSuccess;
            Error = error;
        }

    }

    public class Result
    {
        private Result(bool success, string error = null)
        {
            IsSuccess = success;
            Error = error;
        }

        public bool IsSuccess { get; }
        public string Error { get; }

        public static Result Success()
        {
            return new Result(true);
        }

        public static Result Fail(string error)
        {
            return new Result(false, error);
        }

        public static Result<T> Success<T>(T value)
        {
            return new Result<T>(value);
        }

        public static Result<T> Fail<T>(string error)
        {
            return new Result<T>(false, error);
        }
    }
}