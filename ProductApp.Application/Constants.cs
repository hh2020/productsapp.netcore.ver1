﻿namespace ProductApp.Application
{
    public static class Constants
    {
        public const int DefaultPageSize = 10;
        public const string PageSizeConfigurationName = "PAGE_SIZE";
    }
}