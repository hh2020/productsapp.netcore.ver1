﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.Extensions.Caching.Memory;
using ProductApp.Application.Services;
using Samarasoft.Common.CQRS;

namespace ProductApp.Application.Commands.Product.Create
{
    public class CreateProductCommandHandler: ICommandHandler<CreateProductCommand, Result>
    {
        private readonly IDataStorage _dataStorage;
        private readonly IMemoryCache _memoryCache;
        private readonly ConfigurationService _configurationService;

        public CreateProductCommandHandler(IDataStorage dataStorage, IMemoryCache memoryCache, ConfigurationService configurationService)
        {
            _dataStorage = dataStorage;
            _memoryCache = memoryCache;
            _configurationService = configurationService;
        }

        public Result Handle(CreateProductCommand command)
        {
            _dataStorage.Product.Add(new Domain.Product
            {
                Description = command.Description,
                Name = command.Name,
                Price = command.Price.Value
            });
            _dataStorage.SaveChanges();

            var pageSize = _configurationService.GetPageSize();
            var productsCount = _dataStorage.Product.Count();
            var modulo = productsCount % pageSize;
            if (modulo == 1)
            {
                return Result.Success();
            }

            var keyToRemove = productsCount / pageSize;
            if (modulo != 0)
            {
                keyToRemove++;
            }

            _memoryCache.Remove(keyToRemove.ToString());
            return Result.Success();
        }
    }

    public class CreateProductCommand: ICommand<Result>
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public double? Price { get; set; }
    }
}