﻿using Microsoft.Extensions.Configuration;

namespace ProductApp.Application.Services
{
    public class ConfigurationService
    {
        private readonly IConfiguration _configuration;

        public ConfigurationService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public int GetPageSize()
        {
            return int.TryParse(_configuration[Constants.PageSizeConfigurationName], out var pageSize)
                ? pageSize
                : Constants.DefaultPageSize;
        }
    }
}