﻿using Microsoft.EntityFrameworkCore;
using ProductApp.Domain;
using Samarasoft.Common;

namespace ProductApp.Application
{
    public interface IDataStorage: IMigrator, IUnitOfWork
    {
        DbSet<Product> Product { get; set; }
    }
}