﻿using System.Collections.Generic;
using System.Linq;
using ProductApp.Application.Services;
using Samarasoft.Common.CQRS;

namespace ProductApp.Application.Queries.Product.List
{
    public class ProductListQueryHandler : IQueryHandler<ProductListQuery, Result<ProductListQueryResult>>
    {
        private readonly IDataStorage _dataStorage;
        private readonly ConfigurationService _configurationService;

        public ProductListQueryHandler(IDataStorage dataStorage, ConfigurationService configurationService)
        {
            _dataStorage = dataStorage;
            _configurationService = configurationService;
        }

        public Result<ProductListQueryResult> Ask(ProductListQuery query)
        {
            if (query.PageNumber < 1)
            {
                return Result.Fail<ProductListQueryResult>(Errors.InvalidPage);
            }

            var pageSize = _configurationService.GetPageSize();
            var products = _dataStorage.Product.Skip((query.PageNumber - 1) * pageSize).Take(pageSize)
                .ToList();
            if (!products.Any())
            {
                return Result.Fail<ProductListQueryResult>(Errors.NoProducts);
            }

            var result = new ProductListQueryResult
            {
                Products = products.Select(p => new ProductDto(p.Name, p.Description, p.Price)).ToList()
            };

            return Result.Success(result);
        }
    }
    public class ProductListQuery: IQuery<Result<ProductListQueryResult>>
    {
        public int PageNumber { get; set; }
    }

    public class ProductListQueryResult
    {
        public List<ProductDto> Products { get; set; }
    }

    public class ProductDto
    {
        public ProductDto(string name, string description, double price)
        {
            Name = name;
            Description = description;
            Price = price;
        }

        public ProductDto()
        {

        }

        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
    }
}