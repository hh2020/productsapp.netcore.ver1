﻿namespace ProductApp.Application
{
    public static class Errors
    {
        public const string InvalidPage = "invalid_page";
        public const string NoProducts = "no_products";
    }
}