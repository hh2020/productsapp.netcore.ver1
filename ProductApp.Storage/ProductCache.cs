﻿using System.Collections.Generic;
using System.Linq;

namespace ProductApp.Storage
{
    public class ProductCache
    {
        private readonly Dictionary<string, string> _cache;
        public ProductCache()
        {
            _cache = new Dictionary<string, string>();
        }

        public bool Add(string key, string value)
        {
            return _cache.TryAdd(key, value);
        }

        public bool Get(string key, out string value)
        {
            return _cache.TryGetValue(key, out value);
        }

        public void RemoveLast()
        {
            _cache.Remove(_cache.Last().Key);
        }

        public int Length => _cache.Count;
    }
}