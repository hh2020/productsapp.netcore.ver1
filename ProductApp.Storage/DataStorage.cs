﻿using Microsoft.EntityFrameworkCore;
using ProductApp.Application;
using ProductApp.Domain;

namespace ProductApp.Storage
{
    public class DataStorage: DbContext, IDataStorage
    {
        public DbSet<Product> Product { get; set; }

        public DataStorage(DbContextOptions options) : base(options)
        {

        }

        public void ApplyMigrations()
        {
            Database.Migrate();
        }
    }
}
