﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ProductApp.Application;

namespace ProductApp.Infrastructure
{
    public class StartupMigrator : BackgroundService
    {
        private readonly IServiceProvider _services;

        public StartupMigrator(IServiceProvider services)
        {
            _services = services;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            ApplyMigrations();
            return Task.CompletedTask;
        }

        private void ApplyMigrations()
        {
            using var scope = _services.CreateScope();
            var storage = scope.ServiceProvider.GetService<IDataStorage>();
            storage.ApplyMigrations();
        }
    }
}