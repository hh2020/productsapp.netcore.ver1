﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;

namespace ProductApp.Infrastructure
{
    public class ProductCacheMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IMemoryCache _cache;

        public ProductCacheMiddleware(RequestDelegate next, IMemoryCache cache)
        {
            _next = next;
            _cache = cache;
        }

        public async Task Invoke(HttpContext context)
        {
            if (context.Request.Path.Value == "/product/list" &&
                context.Request.Query.TryGetValue("pageNumber", out var pageNumberFromQuery))
            {
                var pageNumber = pageNumberFromQuery.ToString();
                if (_cache.TryGetValue(pageNumber, out var page))
                {
                    await context.Response.WriteAsync(page.ToString() + " {!}");
                    return;
                }

                await using (var swapStream = new MemoryStream())
                {
                    var originalResponseBody = context.Response.Body;
                    context.Response.Body = swapStream;

                    await _next(context);

                    if (context.Response.StatusCode == 200)
                    {
                        swapStream.Seek(0, SeekOrigin.Begin);
                        var responseBody = new StreamReader(swapStream).ReadToEnd();
                        swapStream.Seek(0, SeekOrigin.Begin);

                        await swapStream.CopyToAsync(originalResponseBody);
                        context.Response.Body = originalResponseBody;

                        _cache.Set(pageNumber, responseBody);
                    }
                }
            }
            else
            {
                await _next(context);
            }
        }
    }
}