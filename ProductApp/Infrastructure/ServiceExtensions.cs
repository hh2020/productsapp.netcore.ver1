﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProductApp.Application;
using ProductApp.Storage;

namespace ProductApp.Infrastructure
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddStorage(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionStrings = GetUserDatabaseConnectionString(configuration);
            services.AddDbContextPool<IDataStorage, DataStorage>(builder =>
            {
                builder.UseNpgsql(connectionStrings);
            }, 16);

            return services;
        }

        private static string GetUserDatabaseConnectionString(IConfiguration configuration)
        {
            return
                $"Host={configuration["DB_HOST"]};Port={configuration["DB_PORT"]};Database={configuration["DB_NAME"]};" +
                $"Username={configuration["DB_USER"]};Password={configuration["DB_PASSWORD"]}";
        }
    }
}