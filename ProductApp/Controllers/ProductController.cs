﻿using Microsoft.AspNetCore.Mvc;
using ProductApp.Application.Commands.Product.Create;
using ProductApp.Application.Queries.Product.List;
using Samarasoft.Common.CQRS;
using Samarasoft.Common.WebApi;

namespace ProductApp.Controllers
{
    [ValidateModelState]
    [Route("product/")]
    public class ProductController: ControllerBase
    {
        private readonly IQueryDispatcher _queryDispatcher;
        private readonly ICommandDispatcher _commandDispatcher;

        public ProductController(IQueryDispatcher queryDispatcher, ICommandDispatcher commandDispatcher)
        {
            _queryDispatcher = queryDispatcher;
            _commandDispatcher = commandDispatcher;
        }

        [HttpGet("list")]
        public ApiResult<ProductListQueryResult> GetProductList([FromQuery]ProductListQuery query)
        {
            var result = _queryDispatcher.Ask(query);
            if (result.IsSuccess)
            {
                return result.Data;
            }

            return ApiResult<ProductListQueryResult>.Fail(result.Error);
        }

        [HttpPost("create")]
        public ApiResult CreateProduct([FromBody] CreateProductCommand command)
        {
            var result = _commandDispatcher.Execute(command);
            if (result.IsSuccess)
            {
                return ApiResult.Ok();
            }

            return ApiResult.Fail(result.Error);
        }
    }
}